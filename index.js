/*

a. What directive is used by Node.js in loading the modules it needs?
	- "require" 

b. What Node.js module contains a method for server creation?
	- http.

c. What is the method of the http object responsible for creating a server using Node.js?
	- createServer()

d. What method of the response object allows us to set status codes and content types?
	- writeHead()

e. Where will console.log() output its contents when run in Node.js?
	- terminal

f. What property of the request object contains the address' endpoint?
	- .url
*/



const http = require('http');

const port = 3000;

const server = http.createServer((request, response) => {

	if(request.url == '/login'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("You are currently in the login page");
	}
	else {
		response.writeHead(404, {'Content-Type': 'text/plain'});
		response.end("Page not available");
	}


})

server.listen(port);

console.log('Server is successfully running at localhost: ${port}');